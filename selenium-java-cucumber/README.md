

** Issues
When integrating juice framework with Selenium cucumber java, I got an error "java.lang.NoSuchMethodError: com.google.common.base.Preconditions.checkState"
which is because of two versions of google-guava in the classpath one from selenium web driver and other from 'juice' framework
Following link helped me to identify the issue
https://github.com/SeleniumHQ/selenium/issues/3880