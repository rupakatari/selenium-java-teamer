package net.ruvetech;

import java.io.IOException;

import org.junit.Test;

import cucumber.api.cli.Main;

public class JunitCucumberRunnerTest {

	@Test
	public void executeAllTests() throws IOException {
		System.out.println("cucumber.options " + System.getProperty("cucumber.options"));
		String cucumberArgs[] = new String[] { "src/main/resources/features/", "-g", "classpath:net/ruvetech/bdd", "-p",
				"pretty", "-p", "html:target/reports", "-s" };
		Main.run(cucumberArgs, Thread.currentThread().getContextClassLoader());
	}

}
