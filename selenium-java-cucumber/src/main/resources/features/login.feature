Feature: Logs into the Teamer application

  @smoke
  Scenario: Successful Login with Valid Credentials
    Given User is on Home Page
    When User Navigate to LogIn Page
    And User enters Credentials to LogIn
      | vvenu88@gmail.com | Test12345 |
    Then Message displayed Login Successfully


  @smoke
  Scenario: Click the drive party page
    Given User logs with credentials vvenu88@gmail.com and Test12345
    When User selects Drive Party team
    Then User is shown Drive Party team page

