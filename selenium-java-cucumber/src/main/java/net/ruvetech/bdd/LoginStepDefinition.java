package net.ruvetech.bdd;

import java.util.List;
import com.google.inject.Inject;
import cucumber.api.PendingException;
import cucumber.runtime.java.guice.ScenarioScoped;
import net.ruvetech.pages.EditAccountPage;
import net.ruvetech.pages.HomePage;
import net.ruvetech.pages.TeamPage;
import net.ruvetech.pages.WelcomePage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@ScenarioScoped
public class LoginStepDefinition {
    private static String HOME_URL = "https://teamer.net/";

    private WelcomePage welcomePage;
    private HomePage homePage;
    private TeamPage teamPage;
    private EditAccountPage editAccountPage;

    private WebDriver driver;

    @Inject
    public LoginStepDefinition(ScenarioContainer scenarioContainer) {
        driver = scenarioContainer.webDriver;
        welcomePage = new WelcomePage(driver);
        homePage = new HomePage(driver);
        teamPage = new TeamPage(driver);
        editAccountPage = new EditAccountPage(driver);
    }

    @Given("^User is on Home Page$")
    public void user_is_on_Home_Page() throws Throwable {
        driver.navigate().to(HOME_URL);
    }

    @When("^User Navigate to LogIn Page$")
    public void user_Navigate_to_LogIn_Page() throws Throwable {
        welcomePage.selectLogin(true);
    }

    @When("^User enters Credentials to LogIn$")
    public void user_enters_Credentials_to_LogIn(DataTable dataTable) throws Throwable {
        List<List<String>> userCredentials = dataTable.raw();
        List<String> firstRow = userCredentials.get(0);
        String userName = firstRow.get(0);
        String password = firstRow.get(1);
        login(userName, password);
    }

    public void login(String userName, String password) {
        welcomePage.login(userName, password);
    }

    @Then("^Message displayed Login Successfully$")
    public void message_displayed_Login_Successfully() throws Throwable {
        System.out.println("User logs in successfully");
    }

    @Given("^User logs with credentials (.*) and (.*)$")
    public void userLogsWithCredentials(String userName, String password) throws Throwable {
        user_is_on_Home_Page();
        user_Navigate_to_LogIn_Page();
        login(userName, password);
    }

    @When("^User selects (.*) team$")
    public void userSelectsTeam(String teamName) throws Throwable {
        homePage.clickTeam(teamName);
    }

    @Then("^User is shown (.*) team page$")
    public void userIsShownTeamPage(String teamName) throws Throwable {
        Assert.assertEquals(teamName, teamPage.getTeamName());
        Thread.sleep(2000);
    }

    @Given("^User enter login credentials (.*) and (.*)$")
    public void userEnterLoginCredentials(String userName,String password) throws Throwable {
        user_is_on_Home_Page();
        user_Navigate_to_LogIn_Page();
        login(userName,password);

    }

    @When("^user selects Upcoming Events link$")
    public void user_selects_Upcoming_Events_link() throws Throwable {
       homePage.clickUpComingEvents();
        driver.navigate().back();
    }

    @When("^user is shown the event page$")
    public void user_is_shown_the_event_page() throws Throwable {
        System.out.println("user is successfully landed into event details page");
    }

    @Then("^user Logout successfully$")
    public void userLogoutSuccessfully() throws Throwable {
        welcomePage.logOutTeamerApplication();

    }
}
