package net.ruvetech.bdd;

import com.google.inject.Inject;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;

public class CucumberApplicationHooks {

    static {
       try {
            String absoluteDirectoryPath = new File(".").getCanonicalPath();
            String path = absoluteDirectoryPath.substring(0, absoluteDirectoryPath.lastIndexOf(File.separator) + 1);
            String driverName = "chromedriver";
            String osName = System.getProperty("os.name").toLowerCase();
            if (osName.indexOf("win") >= 0) {
                driverName = driverName + ".exe";
            }
            System.setProperty("webdriver.chrome.driver", path + driverName);
        } catch (IOException e) {
            //gulp
        }
        System.setProperty("webdriver.chrome.driver","/Users/venugopal/IdeaProjects/selenium-java-teamer/chromedriver");
    }

    @Inject
    private ScenarioContainer scenarioContainer;

    @Before
    public void beforeEveryScenario(Scenario scenario) {
        System.out.println("Feature Name: " + getFeatureName(scenario));
        System.out.println("Scenario Name: " + scenario.getName());
        scenarioContainer.webDriver = new ChromeDriver();
    }

    @After
    public void afterEveryScenario() {
        System.out.println("<<<<<<<<<<<<<<<<<<");
        System.out.println("afterEveryScenario");
      //  scenarioContainer.webDriver.quit();
    }

    private String getFeatureName(Scenario scenario) {
        String featureName = "Feature ";
        String rawFeatureName = scenario.getId().split(";")[0].replace("-", " ");
        featureName = featureName + rawFeatureName.substring(0, 1).toUpperCase() + rawFeatureName.substring(1);

        return featureName;
    }
}
