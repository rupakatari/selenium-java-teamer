package net.ruvetech.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import net.ruvetech.pages.EditAccountPage;
import net.ruvetech.pages.HomePage;
import net.ruvetech.pages.TeamPage;
import net.ruvetech.pages.WelcomePage;

import java.io.File;
import java.io.IOException;

/**
 * Created by vemulav on 20/10/2017.
 */
public class PageObjectModelTests {
	private static String CHROME_DRIVER_NAME = "chromedriver";
	private static String HOME_URL = "https://teamer.net/";
	private static String USER_NAME = "vvenu88@gmail.com";
	private static String USER_PASSWORD = "yellow123";

	private WebDriver driver;

	private WelcomePage welcomePage;
	private HomePage homePage;
	private TeamPage teamPage;
	private EditAccountPage editAccountPage;

	@BeforeClass
	public static void setup() throws IOException {
		String absoluteDirectoryPath = new File(".").getCanonicalPath();
		String path = absoluteDirectoryPath.substring(0, absoluteDirectoryPath.lastIndexOf(File.separator) + 1);
		String driverName = "chromedriver";
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.indexOf("win") >= 0) {
			driverName = driverName + ".exe";
		}
		System.setProperty("webdriver.chrome.driver", path + driverName);
		System.out.println(path + driverName);
	}

	@Before
	public void beforeEveryTest() {
		driver = new ChromeDriver();
		welcomePage = new WelcomePage(driver);
		homePage = new HomePage(driver);
		teamPage = new TeamPage(driver);

		editAccountPage = new EditAccountPage(driver);
	}

	@After
	public void afterEveryTest() {
		// driver.close();
	}

	@Test
	public void testDriveTeamPage() {
		// Given
		driver.navigate().to(HOME_URL);
		welcomePage.selectLogin(true);
		welcomePage.login(USER_NAME, USER_PASSWORD);

		// When
		homePage.clickTeam("Drive Party");

		// Then
		String teamName = teamPage.getTeamName();
		Assert.assertEquals("Drive Party", teamName);
	}

	@Test
	public void testSubscribesForEmailNotification() throws InterruptedException {
		// Given
		driver.navigate().to(HOME_URL);
		welcomePage.selectLogin(true);
		welcomePage.login(USER_NAME, USER_PASSWORD);

		editAccountPage.accountMenuDropDown();
		editAccountPage.editProfile();
		editAccountPage.updateEmailSubscription(true);

		// When
		editAccountPage.save();

		// Then
		// TODO - logic is pending
		Assert.assertTrue(true);

	}

}
