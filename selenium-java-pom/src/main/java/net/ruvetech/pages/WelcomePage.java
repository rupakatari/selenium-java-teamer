package net.ruvetech.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by vemulav on 20/10/2017.
 */
public class WelcomePage extends BasePage {

	@FindBy(how = How.XPATH, using = ".//*[@id='home']/div[3]/div/div/div[2]/div[3]/a")
	private WebElement loginElement;

	@FindBy(how = How.ID, using = "email")
	private WebElement emailInputField;

	@FindBy(how = How.ID, using = "password")
	private WebElement passwordInputField;

	@FindBy(how = How.XPATH, using = ".//*[@id='login_form']/div[4]/button")
	private WebElement logInButton;

	@FindBy(how = How.CLASS_NAME,using="hide-tip-onclick topnavlink nopadding")
	WebElement logoutFieldElement;

	public WelcomePage(WebDriver webDriver) {
		super(webDriver);
		PageFactory.initElements(webDriver, this);
	}

	public void selectLogin(boolean maximizeWindow) {
		//loginElement = driver.findElement(By.xpath(".//*[@id='home']/div[3]/div/div/div[2]/div[3]/a"));
		loginElement.click();

		if (maximizeWindow) {
			driver.manage().window().maximize();
		}

	}
	public void login(String email, String password)
	{
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		emailInputField.sendKeys(email);
		//((JavascriptExecutor)driver).executeScript("document.getElementById('password').value='"+password+"';");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		passwordInputField.sendKeys(password);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logInButton.click();
	}

	public void logOutTeamerApplication(){
		Select drpLogout = new Select(logoutFieldElement);
		drpLogout.selectByVisibleText("Logout");

	Actions actions = new Actions(driver);
	//	actions.moveToElement(logoutFieldElement).perform();

	}
}
