package net.ruvetech.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class BasePage {
	protected WebDriver driver;

	public BasePage(WebDriver webDriver) {
		driver = webDriver;
		PageFactory.initElements(driver, this);
	}
}
