package net.ruvetech.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by vemulav on 20/10/2017.
 */
public class TeamPage extends BaseHomePage {

	@FindBy(how = How.CSS, using = ".text-center.margin-top-small.margin-bottom-small.regular>b")
	private WebElement teamTextField;

	public TeamPage(WebDriver webDriver) {
		super(webDriver);
		PageFactory.initElements(driver, this);
	}

	public String getTeamName() {

		return teamTextField.getText();
	}
}
