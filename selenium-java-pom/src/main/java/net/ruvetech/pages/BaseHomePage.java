package net.ruvetech.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by vemulav on 20/10/2017.
 */
public abstract class BaseHomePage extends BasePage {

	@FindBy(how = How.XPATH, using = ".//*[@id='main-navbar']/div/ul[2]/li[2]/div/ul/li[7]/a")
	private WebElement logoutLinkElement;

	@FindBy(how = How.XPATH, using = ".//*[@id='main-navbar']/div/ul[2]/li[2]/div/a")
	private WebElement accountMenuDropdownElement;

	@FindBy(how = How.XPATH, using = ".//*[@id='main-navbar']/div/ul[2]/li[2]/div/ul/li[2]/a")
	private WebElement editProfileLinkElement;

	public BaseHomePage(WebDriver webDriver) {
		super(webDriver);
		PageFactory.initElements(driver, this);
	}

	public void logout() throws InterruptedException {
		accountMenuDropDown();
		logoutLinkElement.click();
	}

	public void accountMenuDropDown() throws InterruptedException {
		accountMenuDropdownElement.click();
		// TODO - Moved this to app.config
		Thread.sleep(2000);
	}

	public void editProfile() {
		editProfileLinkElement.click();
	}
}
