package net.ruvetech.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by vemulav on 20/10/2017.
 */
public class EditAccountPage extends BaseHomePage {
	@FindBy(how = How.ID, using = "user_opting_in")
	private WebElement phoneMessagesSusbcribeCheckbox;

	@FindBy(how = How.ID, using = "subscription")
	private WebElement emailMessagesSusbcribeCheckbox;

	@FindBy(how = How.XPATH, using = ".//*[@id='user_form']/div[2]/input")
	private WebElement saveAccountButton;

	public EditAccountPage(WebDriver webDriver) {
		super(webDriver);
		PageFactory.initElements(driver, this);
	}

	public void updateEmailSubscription(boolean emailSubscriptionFlag) {

		boolean isChecked = emailMessagesSusbcribeCheckbox.isSelected();
		Actions action = new Actions(driver);
		action.moveToElement(emailMessagesSusbcribeCheckbox).perform();
		if (isChecked != emailSubscriptionFlag) {
			emailMessagesSusbcribeCheckbox.click();
		}
	}

	public void updatePhoneSubscription(boolean phoneSubscriptionFlag) {
		Actions action = new Actions(driver);
		action.moveToElement(phoneMessagesSusbcribeCheckbox).perform();
		boolean isChecked = phoneMessagesSusbcribeCheckbox.isSelected();
		if (isChecked != phoneSubscriptionFlag) {
			phoneMessagesSusbcribeCheckbox.submit();
		}

	}

	public void save() {
		Actions action = new Actions(driver);
		action.moveToElement(saveAccountButton).perform();
		saveAccountButton.submit();

	}
}
