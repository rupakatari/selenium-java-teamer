package net.ruvetech.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by vemulav on 20/10/2017.
 */
public class HomePage extends BasePage {

	@FindBy(how = How.CSS, using = ".link-it-all")
	List<WebElement> teamLinks;

	public HomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public void clickTeam(String teamName) {

		for (WebElement teamLink : teamLinks) {
			String text = teamLink.getText();
			if (text.equalsIgnoreCase(teamName)) {
				teamLink.click();
				return;
			}
		}
		throw new NoSuchElementException("Team with name {0} not found :" + teamName);
	}

	public void clickUpComingEvents(){
		WebElement upComingEvents = driver.findElement(By.xpath("//*[@id=\"teampage_events_container\"]/div[1]/div[2]/a"));
		upComingEvents.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
