import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by vemulav on 19/10/2017.
 */
public class SeleniumTeamerTest {

    private static String CHROME_DRIVER_NAME = "chromedriver";
    private static String HOME_URL = "https://teamer.net/";

    private WebDriver driver;
    private By loginLinkFieldPath = By.xpath(".//*[@id='home']/div[3]/div/div/div[2]/div[3]/a");
    private By emailInputFieldPath = By.id("email");
    private By passwordInputFieldPath = By.id("password");
    private By loginButtonFieldPath = By.xpath(".//*[@id='login_form']/div[4]/button");

    //team page
    private By teamTextFieldPath = By.cssSelector(".text-center.margin-top-small.margin-bottom-small.regular>b");

    private By accountMenuDropdownElementPath = By.xpath(".//*[@id='main-navbar']/div/ul[2]/li[2]/div/a");
    private By editProfileLinkElementPath = By.xpath(".//*[@id='main-navbar']/div/ul[2]/li[2]/div/ul/li[2]/a");

    //edit profile/Users/venugopal/IdeaProjects/selenium-java-teamer/selenium-java-naked/chromedriver.exe
    private By emailMessagesSusbcribeCheckboxPath = By.id("subscription");

    @BeforeClass
    public static void setup() throws IOException {
        String absoluteDirectoryPath = new File(".").getCanonicalPath();
        String path = absoluteDirectoryPath.substring(0, absoluteDirectoryPath.lastIndexOf(File.separator) + 1);
        String driverName = "chromedriver";
        String osName = System.getProperty("os.name").toLowerCase();
        if (osName.indexOf("win") >= 0) {
            driverName = driverName + ".exe";
        }
        System.setProperty("webdriver.chrome.driver", path + driverName);
        System.out.println(path + driverName);
    }

    @Before
    public void beforeEveryTest() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("");
        driver = new ChromeDriver();
    }

    @After
    public void afterEveryTest() {
        //driver.close();
    }

    @Test
    public void testDriveTeamPage() throws InterruptedException {
        // Given
        driver.navigate().to(HOME_URL);
        WebElement loginElement = driver.findElement(loginLinkFieldPath);
        loginElement.click();
        driver.manage().window().maximize();

        driver.findElement(emailInputFieldPath).sendKeys("vvenu88@gmail.com");

        WebElement passwordTextElement = driver.findElement(passwordInputFieldPath);
        //driver.wait(1000);
        //passwordTextElement.sendKeys("yellow123");
        //Thread.sleep(3000);

        //this is how , to use javascript to send value
        ((JavascriptExecutor) driver).executeScript("document.getElementById('password').value='yellow123'");
        driver.findElement(loginButtonFieldPath).click();

        // When
        WebElement driveTeam = getTeamByName(driver, "Drive Party");
        driveTeam.click();

        // Then
        WebElement teamTextField = driver.findElement(teamTextFieldPath);
        Assert.assertEquals("Drive Party", teamTextField.getText());
    }

    @Test
    public void testSubscribesForEmailNotification() throws InterruptedException {
        // Given
        driver.navigate().to(HOME_URL);
        WebElement loginElement = driver.findElement(loginLinkFieldPath);
        loginElement.click();
        driver.manage().window().maximize();

        driver.findElement(emailInputFieldPath).sendKeys("vvenu88@gmail.com");
        driver.findElement(passwordInputFieldPath).sendKeys("yellow123");
        driver.findElement(loginButtonFieldPath).click();
        driver.findElement(accountMenuDropdownElementPath).click();
        Thread.sleep(2000);
        driver.findElement(editProfileLinkElementPath).click();

        //When
        WebElement emailMessagesSusbcribeCheckbox = driver.findElement(emailMessagesSusbcribeCheckboxPath);
        Actions actions = new Actions(driver);
        actions.moveToElement(emailMessagesSusbcribeCheckbox).perform();

    }

    private WebElement getTeamByName(WebDriver driver, String teamName) {
        List<WebElement> teamLinks = driver.findElements(By.cssSelector(".link-it-all"));
        for (WebElement teamLink : teamLinks) {
            String text = teamLink.getText();
            if (text.equalsIgnoreCase(teamName)) {
                return teamLink;
            }
        }
        throw new NoSuchElementException("No Team with name : " + teamName);
    }
}
